//
// Created by gabriel on 07/04/17.
//

#ifndef REPLACEMENT_FRAMETABLE_H
#define REPLACEMENT_FRAMETABLE_H

#include <ostream>
#include <array>
#include "Frame.h"
#include "Process.h"

class FrameTable {
public:
    FrameTable(int, std::string);

    FrameTable();

    void populate(int, const Process);

    const std::list<Frame> &getFrameTable() const;

    void insert(Process, std::vector <std::list<Page>>&, int);

    void remove(Process);

    bool requestFrame(Process process, std::vector <std::list<Page>>&);

    void updateAge();

    void showStatistics();

    bool operator==(const FrameTable &rhs) const;

    bool operator!=(const FrameTable &rhs) const;

    bool operator<(const FrameTable &rhs) const;

    bool operator>(const FrameTable &rhs) const;

    bool operator<=(const FrameTable &rhs) const;

    bool operator>=(const FrameTable &rhs) const;

    friend std::ostream &operator<<(std::ostream &os, const FrameTable &table);

private:
    int totalMiss, totalHit, size;
    std::array<Frame, 20> frameTable;
    std::string algorithm;
};


#endif //REPLACEMENT_FRAMETABLE_H
