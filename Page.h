//
// Created by gabriel on 06/04/17.
//

#ifndef REPLACEMENT_PAGE_H
#define REPLACEMENT_PAGE_H


#include <ostream>

class Page {
public:
    Page(int ID, int cicle);

    Page();

    int getID() const;

    void setID(int ID);

    int getCicle() const;

    void setCicle(int cicle);

    bool operator==(const Page &rhs) const;

    bool operator!=(const Page &rhs) const;

    bool operator<(const Page &rhs) const;

    bool operator>(const Page &rhs) const;

    bool operator<=(const Page &rhs) const;

    bool operator>=(const Page &rhs) const;

    friend std::ostream &operator<<(std::ostream &os, const Page &page);

private:
    int ID,
        cicle;

};


#endif //REPLACEMENT_PAGE_H
