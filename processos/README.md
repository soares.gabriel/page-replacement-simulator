### Estrutura dos arquivos

| ID | Prioridade | Tempo de submissão | Ciclos de I/O | Ciclos totais |
|------|------------|---------------|---------------|-----------|
|  P1  |     1      |      10       |     2         |    0      |
| ...  |   ...      |    ...        |    ...        |   ...     |
| ...  |   ...      |    ...        |    ...        |   ...     |
