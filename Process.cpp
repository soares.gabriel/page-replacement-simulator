//
// Created by gabriel on 22/02/17.
//

#include <iostream>
#include "Process.h"

Process::Process(int ID, int priority, int submission_time, int block_time, int total_time)
        : ID(ID), priority(priority), submission_time(submission_time),
          block_time(block_time), total_time(total_time)
{
    Process::init_execution_time = abs(total_time - block_time);
    Process::execution_time = Process::init_execution_time;
    Process::init_block_time = block_time;
    Process::init_submission_time = submission_time;
    //Process::state = " ";
}

Process::Process() {
    //Process::state = " ";
}


int Process::getPID() const {
    return ID;
}

void Process::setPID(int PID) {
    this->ID = PID;
}

int Process::getPriority() const {
    return priority;
}

void Process::setPriority(int priority) {
    this->priority = priority;
}

int Process::getSubmissionTime() const {
    return submission_time;
}

void Process::setSubmissionTime(int submission_time) {
    this->submission_time = submission_time;
}

int Process::getExecutionTime() const {
    return execution_time;
}

void Process::setExecutionTime(int execution_time) {
    this->execution_time = execution_time;
}

int Process::getBlockTime() const {
    return block_time;
}

void Process::setBlockTime(int block_time) {
    this->block_time = block_time;
}

int Process::getWaitTime() const {
    return wait_time;
}

void Process::setWaitTime(int wait_time) {
    this->wait_time = wait_time;
}

int Process::getResponseTime() const {
    return response_time;
}

void Process::setResponseTime(int response_time) {
    this->response_time = response_time;
}

int Process::getTurnaroundTime() const {
    return turnaround_time;
}

void Process::setTurnaroundTime(int turnaround_time) {
    this->turnaround_time = turnaround_time;
}

int Process::getTotalTime() const {
    return total_time;
}

void Process::setTotalTime(int total_time) {
    this->total_time = total_time;
}

int Process::getInitialSubmissionTime() const {
    return init_submission_time;
}

void Process::setInitialSubmissionTime(int init_submission_time) {
    this->init_submission_time = init_submission_time;
}

int Process::getInitialExecutionTime() const {
    return init_execution_time;
}

void Process::setInitialExecutionTime(int init_execution_time) {
    this->init_execution_time = init_execution_time;
}

int Process::getInitialBlockTime() const {
    return init_block_time;
}

void Process::setInitialBlockTime(int init_block_time) {
    this->init_block_time = init_block_time;
}


int Process::getTickets() const {
    return tickets;
}

void Process::setTickets(int tickets) {
    this->tickets = tickets;
}

int Process::getRatio() const {
    return ratio;
}

void Process::setRatio(int ratio) {
    this->ratio = ratio;
}

void Process::setProcessPageList(const std::list<Page> list) {
    //this->processPageList = list;
    Process::processPageList.assign(list.begin(), list.end());
    //std::copy(list.begin(), list.end(), std::back_insert_iterator<std::list<Page> >(processPageList));
}

std::list <Page> Process::getProcessPageList() {
    return processPageList;
}

void Process::showPageList() {
    for (std::list<Page>::iterator it = Process::processPageList.begin(); it != Process::processPageList.end(); ++it) {
        std::cout << *it << " ";
    }
}

const std::string &Process::getState() const {
    return state;
}

void Process::setState(const std::string &state) {
    Process::state = state;
}

bool Process::operator==(const Process &rhs) const {
    return ID == rhs.ID &&
           priority == rhs.priority &&
           submission_time == rhs.submission_time &&
           execution_time == rhs.execution_time &&
           block_time == rhs.block_time &&
           wait_time == rhs.wait_time &&
           response_time == rhs.response_time &&
           turnaround_time == rhs.turnaround_time &&
           total_time == rhs.total_time &&
           init_submission_time == rhs.init_submission_time &&
           init_execution_time == rhs.init_execution_time &&
           init_block_time == rhs.init_block_time;
}

bool Process::operator!=(const Process &rhs) const {
    return !(rhs == *this);
}

bool Process::operator<(const Process &rhs) const {
    return priority < rhs.priority;
}

bool Process::operator>(const Process &rhs) const {
    return rhs < *this;
}

bool Process::operator<=(const Process &rhs) const {
    return !(rhs < *this);
}

bool Process::operator>=(const Process &rhs) const {
    return !(*this < rhs);
}

Process Process::operator=(const Process &rhs) {
    ID = rhs.ID;
    priority = rhs.priority;
    submission_time = rhs.submission_time;
    execution_time = rhs.execution_time;
    block_time = rhs.block_time;
    wait_time = rhs.wait_time;
    response_time = rhs.response_time;
    turnaround_time = rhs.turnaround_time;
    total_time = rhs.total_time;
    init_submission_time = rhs.init_submission_time;
    init_execution_time = rhs.init_execution_time;
    init_block_time = rhs.init_block_time;
    return *this;
}

std::ostream &operator<<(std::ostream &os, const Process &processo) {
    os << "<ID:" << processo.ID
       << " Pr:" << processo.priority
       << " TEx:" << processo.execution_time
       << " TBl:" << processo.block_time
       << " TSb:" << processo.submission_time << ">";
    return os;
}

Page Process::getFirstPage() {
    if (!Process::processPageList.empty()){
        Page tmp = Process::processPageList.front();
        Process::processPageList.pop_front();
        return tmp;
    }
}
