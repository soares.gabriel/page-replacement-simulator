//
// Created by gabriel on 07/04/17.
//

#ifndef REPLACEMENT_FRAME_H
#define REPLACEMENT_FRAME_H


#include <ostream>
#include "Page.h"

class Frame {
public:
    Frame(bool occupied, const Page &page, int PID);

    Frame(bool occupied, const Page &page, int PID, int lastReference);

    Frame();

    bool isOccupied() const;

    void setOccupied(bool occupied);

    const Page &getPage() const;

    void setPage(const Page &page);

    int getPID() const;

    void setPID(int PID);

    int getAge() const;

    void setAge(int age);

    int getLastReference() const;

    void setLastReference(int);

    void replace(const Frame&);

    bool operator==(const Frame &rhs) const;

    bool operator!=(const Frame &rhs) const;

    bool operator<(const Frame &rhs) const;

    bool operator>(const Frame &rhs) const;

    bool operator<=(const Frame &rhs) const;

    bool operator>=(const Frame &rhs) const;

    friend std::ostream &operator<<(std::ostream &os, const Frame &frame);

private:
    bool occupied;
    Page page;
    int PID,
        age,
        lastReference;
};


#endif //REPLACEMENT_FRAME_H
