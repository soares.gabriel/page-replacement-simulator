//
// Created by gabriel on 06/04/17.
//

#include "Page.h"

Page::Page(int cicle, int ID) :
        ID(ID), cicle(cicle)
{}

Page::Page(){
    ID = 0;
    cicle = 0;
}

int Page::getID() const {
    return ID;
}

void Page::setID(int ID) {
    Page::ID = ID;
}

int Page::getCicle() const {
    return cicle;
}

void Page::setCicle(int cicle) {
    Page::cicle = cicle;
}

bool Page::operator==(const Page &rhs) const {
    return ID == rhs.ID &&
           cicle == rhs.cicle;
}

bool Page::operator!=(const Page &rhs) const {
    return !(rhs == *this);
}

bool Page::operator<(const Page &rhs) const {
    return cicle < rhs.cicle;
}

bool Page::operator>(const Page &rhs) const {
    return rhs < *this;
}

bool Page::operator<=(const Page &rhs) const {
    return !(rhs < *this);
}

bool Page::operator>=(const Page &rhs) const {
    return !(*this < rhs);
}

std::ostream &operator<<(std::ostream &os, const Page &page) {
    os << page.cicle << ":" << page.ID;
    return os;
}
