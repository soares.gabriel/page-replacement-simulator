# Sistemas Operacionais - 2016.2
## Projeto 2 - Algoritmos de Substituição de Páginas

Neste projeto serão implementados quatro algoritmos de substituição de páginas (listados abaixo). O algoritmo de escalonamento a ser utilizado deve ser o _Round Robin_ com _quantum_ igual a 2. Ao final da simulação, a quantidade de faltas de páginas deve ser informada para cada algoritmo.


Algoritmos de substituição de páginas:
- _Optimal_
- _Least recently used (LRU)_
- _First-in-first-out (FIFO)_
- _Clock_

### Cenários de simulação

Os algoritmos devem ser executados utilizando três cenários que serão fornecidos contendo conjuntos de processos bem como as sequencias de referencias de páginas de cada processo. Informações sobre as referencias das páginas serão fornecidas da seguinte forma:

_id do processo, tempo:página, tempo:página, tempo:página, .., tempo:página_
_id do processo, tempo:página, tempo:página, tempo:página, .., tempo:página_
_id do processo, tempo:página, tempo:página, tempo:página, .., tempo:página_

**Exemplo:** considerando que o processo de id 1 executará por 290 unidades de tempo (tempo total de execução menos o tempo de bloqueio), a seguinte linha indicará que no instante 0 será feita referencia a página __X__, depois no instante 10 a página __Y__, depois no instante 15 a página __X__ novamente, etc. 

_1, 0:__X__, 10:__Y__, 15:__X__, 20:__Z___, etc.

A qualquer instante de tempo durante a simulação, o grau de multiprogramação do sistema deve ser 100. A quantidade de quadros de memória disponível será 40.

### Resultados

Os resultados devem ser exibidos em tabelas e gráficos para cada cenário de simulação.
