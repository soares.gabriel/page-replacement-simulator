//
// Created by gabriel on 29/03/17.
//

#include <iostream>
#include "RoundRobin.h"
#include "Metrics.h"
#include "FrameTable.h"

#define WAIT_QUEUE 0
#define BLOCK_QUEUE 1

using namespace std;

void printProcessQueue(std::list<Process> queue)
{
    for (std::list<Process>::iterator it=queue.begin(); it != queue.end(); ++it)
        cout << *it << " ";
}

void printFirstProcess(std::list<Process> queue)
{
    for (std::list<Process>::iterator it=queue.begin(); it != queue.end(); ++it){
        cout << *it << " ";
        break;
    }
}
bool compare_processes_block_times(const Process& first, const Process& second)
{ return (first.getBlockTime() < second.getBlockTime()); }

bool compare_processes_submission_times(const Process& first, const Process& second)
{ return (first.getSubmissionTime() < second.getSubmissionTime()); }

std::list <Process> updateBlockQueue(std::list < Process >* blockQueue) {
    blockQueue->sort(compare_processes_block_times);

    //cout << "\t\t\t\t[Start] updateBlockQueue main loop" << endl;
    std::list<Process>::iterator i = blockQueue->begin();
    std::list<Process> proc_remover;

    while (i != blockQueue->end()) {
        if (i->getBlockTime() == 0) {
//            //cout << "\t\t\t\t\t[Info]Moving " << (*i) << " to WAIT" << endl;
            proc_remover.push_back((*i));
            blockQueue->remove(*i++);
        } else {
            //cout << "\t\t\t\t\t[Info] *it points to " << (*i) << endl;
            i->setBlockTime(i->getBlockTime() - 1);
            //cout << "\t\t\t\t\t[Info] " << (*i) << "-- in WAIT" << endl;
        }
        i++;
    }

    blockQueue->sort(compare_processes_block_times);

    //cout << "\t\t\t\t[Done] updateBlockQueue main loop" << endl;
    return proc_remover;
}

std::list <Process> updateWaitQueue(std::list < Process >* waitQueue) {
    waitQueue->sort(compare_processes_submission_times);

    //cout << "\t\t\t\t[Start] updateWaitQueue main loop" << endl;
    std::list<Process>::iterator i = waitQueue->begin();
    std::list<Process> proc_remover;

    while (i != waitQueue->end())
    {
        if (i->getSubmissionTime() == 0) {
            //cout << "\t\t\t\t\t[Info] removing " << (*i) << "from WAIT" << endl;
            proc_remover.push_back((*i));
            waitQueue->remove(*i++);
        }
        else {
            //cout << "\t\t\t\t\t[Info] *it points to " << (*i) << endl;
            i->setSubmissionTime(i->getSubmissionTime() - 1);
            //cout << "\t\t\t\t\t[Info] " << (*i) << "-- in WAIT" << endl;
        }
        i++;
    }

    waitQueue->sort(compare_processes_submission_times);

    //cout << "\t\t\t\t[Done] updateWaitQueue main loop" << endl;
    return proc_remover;
}

void updateSuspendQueue(std::list < Process > suspendQueue,
                        std::list < Process > waitQueue,
                        std::list < Process > blockQueue,
                        FrameTable& primaryMemory, int sum,int alpha){

    //cout << "\t\t\t\t[Start] updateSuspendQueue main loop" << endl;
    suspendQueue.sort(compare_processes_block_times);
    std::list<Process>::iterator i = suspendQueue.begin();

    if (!suspendQueue.empty()) {
        for (std::list<Process>::iterator it = suspendQueue.begin(); it != suspendQueue.end(); ++it) {
            if (it->getBlockTime() > 0) {
                it->setBlockTime(it->getBlockTime() - 1);
            }
        }
    }

    if ((sum == alpha) &&
            (waitQueue.front().getSubmissionTime() == 1) &&
            (suspendQueue.empty() &&
            ((blockQueue.front().getPID() < 100))) &&
            (blockQueue.front().getPID() > 0)){
        //cout << "\t\t\t\t\t[Info] moving process " << (blockQueue->front()) << " from BLOCK to SUSPEND" << endl;
        suspendQueue.push_back((blockQueue.front()));
        blockQueue.pop_front();

        cout << "Removendo páginas de " << suspendQueue.back() << " SUSPENDED"  << endl;

        primaryMemory.remove(suspendQueue.back());

        //cout << primaryMemory << endl;

//        for (std::list<Process>::iterator it=suspendQueue->begin(); it != suspendQueue->end(); ++it)
//            //cout << "Fila de suspenso" << *it << " ";
    }

    if (!waitQueue.empty()) {
        if ((suspendQueue.front().getBlockTime() == 0) && (waitQueue.front().getSubmissionTime() == 0)) {
            suspendQueue.front().setSubmissionTime(1);
            //cout << "\t\t\t\t\t[Info] moving process " << (suspendQueue->front()) << " from SUSPEND to WAIT[1] " << endl;
            waitQueue.push_back((Process &&) (suspendQueue.front()));
            suspendQueue.pop_front();
        }
    }
    else if ((suspendQueue.front().getBlockTime() == 0) && (waitQueue.front().getSubmissionTime() != 0))
    {
        //cout << "\t\t\t\t\t[Info] moving process " << (suspendQueue->front()) << " from SUSPEND to WAIT[0] " << endl;
        waitQueue.push_back((Process &&) (suspendQueue.front()));
        suspendQueue.pop_front();
    }

    //cout << "\t\t\t\t[Done] updateSuspendQueue main loop" << endl;
}

void RoundRobin::run(int alpha, int quantum, std::list < Process > processes, std::vector <std::list<Page>> processPagesVector)
{
    //cout << "[Start] "<< "" << "Round Robin scheduling" <<"" << endl;

    int cpuActive = 0,
        originalQuantum = quantum,
        totalProcesses = processes.size(),
        waitTimeSum = 0,
        responseTimeSum = 0,
        turnaroundTimeSum = 0,
        cicles = 0;

    std::list <Process> waitQueue,
                        blockQueue,
                        readyQueue,
                        suspendQueue,
                        blockedReadyProcess,
                        waitingReadyProcess,
                        runningProcess;

    Process tmp, nextCicle;

    FrameTable primaryMemory(20, "FIFO");
    std::list <Frame> secondaryMemory;

    while (!processes.empty())
    {
        tmp = processes.front();
        if (tmp.getSubmissionTime() > 0)
            waitQueue.push_back(tmp);
        else
            readyQueue.push_back(tmp);

        processes.pop_front();
    }

    //cout << "\t[Info] process distribution WAIT or READY" << endl;

    waitQueue.sort(compare_processes_submission_times);

    //cout << "\t[Info] submission processes sorted" << endl;

    //cout << "\t[Start] RoundRobin::run main loop" << endl;
    int sum = 0, diff = 0, fault = 0, hit = 0, tmp_sum = 0;
    bool flagNextCicle = false;
    while (true)
    {
        //cout << "" << "\t\t[Info] cicle: " << cicles << "" << endl;

        //cout << "" << "\t\t[Info] Quantum: " << quantum << "" << endl;

        sum = blockQueue.size() + readyQueue.size() + runningProcess.size() + blockedReadyProcess.size();

        //cout << "" << "\t\t[Info] Soma: " << sum << " = " << blockQueue.size() << "(BLOCK) + " << readyQueue.size() << "(READY) + " << runningProcess.size() << "(EXEC)"<< endl;

//        cout << "\t\t[Info] running process ";
//        if (!runningProcess.empty()) printFirstProcess(runningProcess); else cout << "-";
//        cout << endl;

        //cout << "\t\t[Info] READY queue ";
        //if (!readyQueue.empty())  printProcessQueue(readyQueue); else //cout << "-";
        //cout << endl;

        //cout << "\t\t[Info] BLOCK queue ";
        //if (!blockQueue.empty()) printProcessQueue(blockQueue); else //cout << "-";
        //cout << endl;

        //cout << "\t\t[Info] SUSPEND queue ";
        //if (!suspendQueue.empty()) printFirstProcess(suspendQueue); else //cout << "-";
        //cout << endl;

        //cout << "\t\t[Info] WAIT queue ";
        //if (!waitQueue.empty()) printFirstProcess(waitQueue); else //cout << "-";
        //cout << endl;

//        if (flagNextCicle){
//            blockQueue.push_back(nextCicle);
//            flagNextCicle = false;
//        }


        //cout << "\t\t\t[Info] updating BLOCK queue" << endl;
        blockedReadyProcess = updateBlockQueue(&blockQueue);
        if (!blockQueue.empty()) {
            while (!blockedReadyProcess.empty()) {
                readyQueue.push_back((Process &&) blockedReadyProcess.front());
                blockedReadyProcess.pop_front();
                /*
                 *  Quando um processo é criado e adicionado a fila de prontos, a
                 *  sua página zero deve ser alocada em memória principal.
                 */


                cout << "Adicionando página zero de " << readyQueue.back()
                     << " tamanho lista pag: " << processPagesVector[readyQueue.back().getPID() - 1].size() << endl;

                primaryMemory.insert(readyQueue.back(), processPagesVector, 0);
                //cout << primaryMemory << endl;
            }
        }

        if ((!runningProcess.empty()) && (flagNextCicle)) {
            //cout << "\t\t\t[Info]" << runningProcess.front() << "BLOCKED";
            blockQueue.push_back((Process &&) runningProcess.front());

//                nextCicle = (Process &&) runningProcess.front();
//                flagNextCicle = true;
//                printProcessQueue(blockQueue);
            //cout << endl;
            runningProcess.pop_front();
            flagNextCicle = false;
            quantum = originalQuantum;
        }

        if ((!runningProcess.empty()) && (quantum > 0)) {
            quantum--;

            if (runningProcess.front().getExecutionTime() > 0) {
                if (!processPagesVector[runningProcess.front().getPID()].empty()) {

                    if (processPagesVector[runningProcess.front().getPID() - 1].front().getCicle() ==
                        abs(runningProcess.front().getExecutionTime() - runningProcess.front().getInitialExecutionTime())) {

                        if (!primaryMemory.requestFrame(runningProcess.front(), processPagesVector)) {

                            cout << " page FAULT" << endl; ++fault;

                            cout << "\nRequerendo páginas de " << runningProcess.front() << " tamanho lista pag: "
                                 << processPagesVector[runningProcess.front().getPID() - 1].size() << endl;
                        } else
                            cout << " page HIT" << endl; ++hit;
                    }
                }

                runningProcess.front().setExecutionTime(runningProcess.front().getExecutionTime() - 1);

                if (runningProcess.front().getBlockTime() > 0)
                    flagNextCicle = true;
            }
        }
        //cout << "\t\t\t[Info] updating SUSPEND queue" << endl;
        updateSuspendQueue(suspendQueue, waitQueue, blockQueue, primaryMemory, sum, alpha);

        // if (!waitQueue.empty()) {
        //     //cout << "\t\t\t[Info] updating WAIT queue" << endl;
        //     //cout << "\t\t\t[Info] inside waitingReadyProcess ";
        //     if (!waitingReadyProcess.empty())
        //         waitingReadyProcess.front();
        //     else
        //         //cout << "-";
        //     //cout << endl;

        if ((!runningProcess.empty())
            && (runningProcess.front().getExecutionTime() == 0)
            && (runningProcess.front().getBlockTime() == 0)) {

            Metrics::updateProcessMetrics(runningProcess.front(), waitTimeSum, responseTimeSum, turnaroundTimeSum, cicles);
            //cout << "" << "\t\t\t[Info] " << runningProcess.front() << " FINISHED " << "";
            Metrics::showMetricsFinishedProcess(runningProcess.front());

            cout << "Removendo páginas de " << runningProcess.front()
                 << " tamanho lista pag: " << processPagesVector[runningProcess.front().getPID() - 1].size() << endl;

            primaryMemory.remove(runningProcess.front());

            runningProcess.pop_front();
            quantum = originalQuantum;
        }

        if ((!runningProcess.empty())
            && (!readyQueue.empty())
            && (quantum == 0)){

            readyQueue.push_back((Process &&) runningProcess.front());
            runningProcess.pop_front();
            quantum = originalQuantum;
        }

        //cout << "\t\t\t[Info] Updating Processor..." << endl;
        if (!readyQueue.empty() && runningProcess.empty()) {
            //cout << "\t\t\t\t[Info] Moving " << readyQueue.front() << " to Processor." << endl;
            runningProcess.push_back((Process &&) readyQueue.front());
            readyQueue.pop_front();
            quantum = originalQuantum;

//            if (runningProcess.front().getResponseTime() == -1)
//                runningProcess.front().setResponseTime(cicles - runningProcess.front().getInitialSubmissionTime());
        }
        //cout << "\t\t\t[Info] Updating WAIT queue..." << endl;
        waitingReadyProcess = updateWaitQueue(&waitQueue);

        while (!waitingReadyProcess.empty()) {
            diff = alpha - sum;

            if ((waitingReadyProcess.size() <= diff) && (sum < alpha)) {

                readyQueue.push_back((Process &&) waitingReadyProcess.front());
                waitingReadyProcess.pop_front();

                cout << "Adicionando página zero de " << readyQueue.back()
                     << " tamanho lista pag: " << processPagesVector[readyQueue.back().getPID() - 1].size() << endl;


                primaryMemory.insert(readyQueue.back(), processPagesVector , 0);
                //cout << primaryMemory << endl;

//                //cout << "\t\t\t[Info] inside waitingReadyProcess: push_back into READY" << waitingReadyProcess.front() << endl;
            }
            else {
                for (std::list<Process>::iterator it = waitingReadyProcess.begin(); it != waitingReadyProcess.end(); ++it) {

                    if ((it->getPID() > 100) || (it->getPID() < 0))
                        break;

                    if (it->getSubmissionTime() == 0) {
                        it->setSubmissionTime(1);
                    }
//                    //cout << "\t\t\t\t|Processo " << it->getPID() << " |Tempo de Subm: " << it->getSubmissionTime() << "|\n" << endl;
                    waitQueue.push_back(*it);

                    if (it == waitingReadyProcess.end()) {
                        waitingReadyProcess.remove(*it);
                        break;
                    }
                    else
                        waitingReadyProcess.remove(*it);

//                    //cout << "\t\t\t\t|Fila de Espera-Pronto: ";
//                    printProcessQueue(waitingReadyProcess);
//                    //cout << endl;
                }
            }
        }

        if (blockQueue.empty() && readyQueue.empty() && waitQueue.empty() && runningProcess.empty())
            break;

        if ((sum ==  1) && (waitQueue.empty()) && (quantum == 0)){
            quantum = originalQuantum;
        }
        cicles++;
        primaryMemory.updateAge();
        cout << primaryMemory << endl;

    }
    //cout << "\t[Done] RoundRobin::run main loop" << endl;
    Metrics::showAlgorithmMetrics(waitTimeSum, responseTimeSum, totalProcesses, turnaroundTimeSum, cicles, cpuActive);
    primaryMemory.showStatistics();

    cout << "HIT: " << hit << " FAULT: " << fault << " total: " << hit + fault << endl;

    //cout << "[Done] "<< "" << "Round Robin scheduling" << "" << endl;

    for (auto it = processPagesVector.begin(); it != processPagesVector.end(); ++it) {
        tmp_sum += (*it).size();
    }
    cout << tmp_sum << endl;
}
