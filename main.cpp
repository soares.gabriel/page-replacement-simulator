#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <list>
#include <algorithm>
#include <boost/algorithm/string.hpp>

#include "Process.h"
#include "fast-cpp-csv-parser/csv.h"        /* https://github.com/ben-strasser/fast-cpp-csv-parser */
#include "RoundRobin.h"
#include "Page.h"

using namespace std;

void help()
{
    cout << "\n\t# Simulador de algoritmos de substituição de página #\n\tUso: programa [INPUT FILE] [INPUT FILE] [OUTPUT FILE]\n\n";
}

int main(int argc, char **argv) {

    list < Process > proc_data;                                  /* processes from file */
    int ID, submission_time, priority, total_time, block_time;      /* process info */

    std::vector<std::string> tmpStrLine;
    std::vector<std::string> tmpStrPageRef;

    std::vector <std::list<Page>> processPagesVector;

    std::string::size_type sz;

    if ( argc != 4 ){
        help();
    } else {

        ifstream input_cen(argv[1]);
        ifstream input_ref(argv[2]);
        ofstream output(argv[3]);

        if ( !input_cen.is_open() && !input_ref.is_open() && !output.is_open())
            cout <<"\n\nNao foi possivel abrir o(s) arquivo(s) de entrada e/ou saida.\n\n";
        else {
            io::CSVReader<5> in_cen(argv[1]);
            while(in_cen.read_row(ID, submission_time, priority, total_time, block_time)){

                Process tmp(ID, priority, submission_time, block_time, total_time);
                tmp.setState("WAIT");
                proc_data.push_back(tmp);
            }
            int i = 0;
            std::list<Process>::iterator it_proc_data = proc_data.begin();

            io::LineReader in_ref(argv[2]);
            while(char*line = in_ref.next_line())
            {
                string tmp = string(line);

                boost::split(tmpStrLine, tmp, boost::is_any_of(","));
                std::list<Page> processPagesList;

                for (std::vector<string>::iterator it = tmpStrLine.begin(); it != tmpStrLine.end(); ++it) {

                    if ((*it).find(':') != std::string::npos) {
                        boost::split(tmpStrPageRef, *it, boost::is_any_of(":"));
                        Page tmpPage(stoi(tmpStrPageRef[0], &sz), stoi(tmpStrPageRef[1], &sz));
                        processPagesList.push_back(tmpPage);
                    }
                }
                processPagesList.sort();
                processPagesVector.push_back(processPagesList);
                (*it_proc_data).setProcessPageList(processPagesList);
                it_proc_data++;
            }

//            for (std::list<Process>::iterator it = proc_data.begin(); it != proc_data.end(); ++it) {
//                cout << *it << " "; (*it).showPageList(); cout << endl;
//            }

//            for (auto it = processPagesVector.begin(); it != processPagesVector.end(); ++it) {
//                for (auto it1 = (*it).begin(); it1 != (*it).end(); ++it1){
//                    cout << *it1 << " ";
//                }
//                cout  << endl;
//            }

            cout << "[Info]\e[1m"<<" All processes LOADED from: "<<argv[1] <<"\e[0m" << endl;
            RoundRobin::run(50, 2, proc_data, processPagesVector);

        }
    }

    return EXIT_SUCCESS;

}
