//
// Created by gabriel on 07/04/17.
//

#include "Frame.h"

Frame::Frame(bool occupied, const Page &page, int PID)
        : occupied(occupied), page(page), PID(PID) {
    age = 0;
    lastReference = 0;
}

Frame::Frame(bool occupied, const Page &page, int PID, int lastReference)
        : occupied(occupied), page(page), PID(PID), lastReference(lastReference) {
    age = 0;
    lastReference = 0;
}

Frame::Frame(){
    PID = 0;
    occupied = false;
    age = 0;
    lastReference = 0;
    page.setCicle(0);
    page.setID(0);
}

bool Frame::isOccupied() const {
    return occupied;
}

void Frame::setOccupied(bool occupied) {
    Frame::occupied = occupied;
}

const Page &Frame::getPage() const {
    return page;
}

void Frame::setPage(const Page &page) {
    Frame::page = page;
}

int Frame::getPID() const {
    return PID;
}

void Frame::setPID(int PID) {
    Frame::PID = PID;
}

int Frame::getAge() const
{
    return age;
}

void Frame::setAge(int age)
{
    Frame::age = age;
}

int Frame::getLastReference() const
{
    return lastReference;
}

void Frame::setLastReference(int lastRef)
{
    Frame::lastReference = lastRef;
}

bool Frame::operator==(const Frame &rhs) const {
    return occupied == rhs.occupied &&
           page == rhs.page &&
           PID == rhs.PID;
}

bool Frame::operator!=(const Frame &rhs) const {
    return !(rhs == *this);
}

bool Frame::operator<(const Frame &rhs) const {
    if (occupied < rhs.occupied)
        return true;
    if (rhs.occupied < occupied)
        return false;
    if (page < rhs.page)
        return true;
    if (rhs.page < page)
        return false;
    return PID < rhs.PID;
}

bool Frame::operator>(const Frame &rhs) const {
    return rhs < *this;
}

bool Frame::operator<=(const Frame &rhs) const {
    return !(rhs < *this);
}

bool Frame::operator>=(const Frame &rhs) const {
    return !(*this < rhs);
}

std::ostream &operator<<(std::ostream &os, const Frame &frame) {
    if (frame.occupied)
        os << "[IN USE" << " page: " << frame.page << " PID: " << frame.PID << " age: " << frame.age << "]";
    else
        os << "[FREE" << " page: " << frame.page << " PID: " << frame.PID << " age: " << frame.age << "]";
    return os;
}

void Frame::replace(const Frame & frame) {
    Frame::occupied = frame.isOccupied();
    Frame::page = frame.getPage();
    Frame::PID = frame.getPID();
    Frame::age = frame.getAge();
    Frame::lastReference = frame.getLastReference();
}
