//
// Created by gabriel on 07/04/17.
//

#include <iostream>
#include <algorithm>
#include "FrameTable.h"
#include "Process.h"


void update_age_time(Frame& frame) { frame.setAge(frame.getAge() + 1); }
bool free_frame(const Frame& frame) { return !frame.isOccupied(); }
bool compare_frame_age(const Frame& first, const Frame& second) { return (first.getAge() < second.getAge()); }
bool compare_frame_last_ref(const Frame& first, const Frame& second) { return (first.getLastReference() < second.getLastReference()); }
bool check_page_zero(const Page& page) { return (page.getID() == 0); }

FrameTable::FrameTable(int size, std::string alg)
        : algorithm(alg)
{
    totalHit = 0;
    totalMiss = 0;
}

FrameTable::FrameTable() {
    totalHit = 0;
    totalMiss = 0;
}

void FrameTable::populate(int cicle, Process process){
    Page pg0(cicle, process.getPID());
    Frame tmp1(true, pg0, process.getPID());

    for(int i = 0; i < frameTable.size(); i++)
        if (!frameTable.at(i).isOccupied()) {
            frameTable[i] = tmp1;
            break;
        } else {
            auto tmp = (*std::max_element(frameTable.begin(), frameTable.end(), compare_frame_age));
            tmp.setOccupied(true);
            tmp.setAge(0);
            tmp.setPID(process.getPID());
            tmp.setPage(pg0);
            break;
        }
}


void FrameTable::insert(Process process, std::vector <std::list<Page>>& processPagesVector, int zero)
{
    std::cout << "Inserindo página " << processPagesVector[process.getPID() - 1].front() << " de " << process << std::endl;

        for (int i = 0; i < frameTable.size(); i++)
            if (!frameTable[i].isOccupied()) {
                Frame frame(true, processPagesVector[process.getPID() - 1].front(), process.getPID());
                processPagesVector[process.getPID() - 1].pop_front();
                frameTable[i].replace(frame);
                frameTable[i].setAge(0);
                break;

            } else if ((i == frameTable.size() - 1)
                       && frameTable[i].isOccupied()) {

                auto tmp = std::max_element(frameTable.begin(), frameTable.end(), compare_frame_age);
                Frame frame(true, processPagesVector[process.getPID() - 1].front(), process.getPID());
                processPagesVector[process.getPID() - 1].pop_front();
                (*tmp).replace(frame);
                (*tmp).setAge(0);
                break;
            }
}

void FrameTable::remove(Process process) {
    for (auto it = this->frameTable.begin(); it != this->frameTable.end(); ++it)
        if ((*it).getPID() == process.getPID() && (*it).isOccupied())
            (*it).setOccupied(false);
}

const std::list<Frame> &FrameTable::getFrameTable() const {
    return (const std::list<Frame> &) frameTable;
}

bool FrameTable::requestFrame(Process process, std::vector <std::list<Page>>& processPagesVector){

    std::cout << "\n\t\t" << process << " requesting page " << processPagesVector[process.getPID() - 1].front();

    for (auto it = frameTable.begin(); it != frameTable.end(); ++it){
        if (((*it).getPID() == process.getPID())
            && (*it).isOccupied()
            && (*it).getPage().getID() == processPagesVector[process.getPID() - 1].front().getCicle()
                )
        {
            std::cout <<  " HIT" << std::endl;
            FrameTable::totalHit++;
            return true;
        } else if (it == frameTable.end()){
            std::cout << " FAULT" << std::endl;
            FrameTable::totalMiss++;
            FrameTable::insert(process, processPagesVector, 1);
            return false;
        }
    }
}

void FrameTable::updateAge()
{
    for (auto it = frameTable.begin(); it != frameTable.end(); ++it)
        if ((*it).isOccupied())
            (*it).setAge((*it).getAge() + 1);
}

void FrameTable::showStatistics() {
    std::cout << "Frame Table Stats" << std::endl
              << "total HITs: " << FrameTable::totalHit << std::endl
              << "total PAGE FAULT: " << FrameTable::totalMiss << std::endl;
}


bool FrameTable::operator==(const FrameTable &rhs) const {
    return frameTable == rhs.frameTable;
}

bool FrameTable::operator!=(const FrameTable &rhs) const {
    return !(rhs == *this);
}

bool FrameTable::operator<(const FrameTable &rhs) const {
    return frameTable < rhs.frameTable;
}

bool FrameTable::operator>(const FrameTable &rhs) const {
    return rhs < *this;
}

bool FrameTable::operator<=(const FrameTable &rhs) const {
    return !(rhs < *this);
}

bool FrameTable::operator>=(const FrameTable &rhs) const {
    return !(*this < rhs);
}

std::ostream &operator<<(std::ostream &os, const FrameTable &table) {
    int i = 0;
    for (auto it = table.frameTable.begin(); it != table.frameTable.end(); ++it) {
        ++i;
        os << i << *it << std::endl;
    }
    return os;
}


