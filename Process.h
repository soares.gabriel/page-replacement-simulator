//
// Created by gabriel on 22/02/17.
//

#ifndef ALGORITMOS_DE_ESCALONAMENTO_PROCESSO_H
#define ALGORITMOS_DE_ESCALONAMENTO_PROCESSO_H


#include <ostream>
#include <list>
#include <vector>
#include "Page.h"

class Process {
public:
    /* Constructor */
    Process(int ID, int priority, int submission_time, int block_time, int total_time);
    Process();

    /* Getters and Setters */
    int getPID() const;
    void setPID(int ID);
    int getPriority() const;
    void setPriority(int priority);
    int getSubmissionTime() const;
    void setSubmissionTime(int submission_time);
    int getExecutionTime() const;
    void setExecutionTime(int execution_time);
    int getBlockTime() const;
    void setBlockTime(int block_time);
    int getWaitTime() const;
    void setWaitTime(int wait_time);
    int getResponseTime() const;
    void setResponseTime(int response_time);
    int getTurnaroundTime() const;
    void setTurnaroundTime(int turnaround_time);
    int getTotalTime() const;
    void setTotalTime(int total_time);
    int getInitialSubmissionTime() const;
    void setInitialSubmissionTime(int init_submission_time);
    int getInitialExecutionTime() const;
    void setInitialExecutionTime(int init_execution_time);
    int getInitialBlockTime() const;
    void setInitialBlockTime(int init_block_time);

    int getTickets() const;
    void setTickets(int tickets);
    int getRatio() const;
    void setRatio(int ratio);

    void showPageList();

    bool operator<(const Process &rhs) const;

    bool operator>(const Process &rhs) const;

    bool operator<=(const Process &rhs) const;

    bool operator>=(const Process &rhs) const;

    friend std::ostream &operator<<(std::ostream &os, const Process &processo);

    bool operator==(const Process &rhs) const;

    bool operator!=(const Process &rhs) const;

    Process operator=(const Process &rhs);

    void setProcessPageList(const std::list<Page>);

    std::list <Page> getProcessPageList();

    Page getFirstPage();

    const std::string &getState() const;

    void setState(const std::string &state);


private:
        int ID,
            priority,
            submission_time,
            execution_time,
            block_time,
            wait_time,
            response_time,
            turnaround_time,
            total_time,
            init_submission_time,
            init_execution_time,
            init_block_time,
            tickets,
            ratio;

        std::string state;

        std::list <Page> processPageList;

};


#endif //ALGORITMOS_DE_ESCALONAMENTO_PROCESSO_H
