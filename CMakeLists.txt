cmake_minimum_required(VERSION 3.6)
project(replacement)
find_package (Threads)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp Process.cpp Process.h RoundRobin.cpp RoundRobin.h Metrics.cpp Metrics.h Page.cpp Page.h Frame.cpp Frame.h FrameTable.cpp FrameTable.h)
add_executable(replacement ${SOURCE_FILES})
target_link_libraries (replacement ${CMAKE_THREAD_LIBS_INIT})
